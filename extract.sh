#!/bin/bash
#: Title       : Android Reverse Engineering scritp
#: Date        : 2017-05-10
#: Author      : "Adrià Prat" <adria@adriaprat.com>
#: Version     : 1.0
#: Description : This bash script extracts the apk and subsequent jar from an android device
#: Options     : app name as a parameter

error_message=""

##############
# Validations
##############

# check if adb is installed
hash adb 2>/dev/null || { 
	error_message="    - adb\n";
}

# check if dex2jar.sh is present
hash dex2jar-2.0/d2j-dex2jar.sh 2>/dev/null || { 
	error_message+="$error_message   - ./dex2jar-2.0/d2j-dex2jar.sh has to be installed in this directory but it's not installed. You can download it from the original repository, https://bitbucket.org/pxb1988/dex2jar/downloads/ \n";
}
 
# check if aapt is installed
hash aapt 2>/dev/null || {
	error_message="$error_message   -aapt: aapt is located in /Users/$USER/Library/Android/sdk/build-tools/{build tools versions}/aapt.\n	In order to use it you must export aapt path doing $ export PATH=\"\$PATH:/Users/$USER/Library/Android/sdk/build-tools/{build tools versions}\"\n";
}

# check if apktool is installed
hash apktool 2>/dev/null || {
    error_message="$error_message   -apktool: You can check the installation procedure here, http://ibotpeaches.github.io/Apktool/install/ \n";
}

# if any command is missing, abort
if [[ ! -z "$error_message" ]]
    then
        echo -e >&2 "Required commands:\n$error_message\nAborting.";
        exit 1;
fi

# check if has one parameter
if [ $# -eq 0 ] 
	then
		adb shell pm list packages -f
    	echo "No arguments supplied. Supply the app name you want to extract.";
		exit 0;
fi
##################
# End validations
##################

# list packages and grep result with the parameter
adb shell pm list packages -f | grep "$1";

# wait for input and save it into a variable
echo -e "\n✍️  Enter package path with apk to extract: ";
read -r path

# use app identifier as the app name
app_name=$(echo "$path" | cut -d'/' -f 4)

apk_name="base.apk"
apk_path="$app_name/$apk_name";
jar_path="$app_name/base.jar";

# create dir with variable name
mkdir "$app_name"
echo "";

# extract apk from user input and save it in previously created directory
if adb pull -p "$path" "$apk_path"
    then
    ./dex2jar-2.0/d2j-dex2jar.sh -f -o "$jar_path" "$apk_path"
	echo -e "👍  apk and jar extracted.\n"
else 
	echo "‼️  adb failed."
	exit 1;
fi

# extract package content with aapt
package_content_file=$app_name"/package_contents.txt"
echo "⏳ Extracting package content in $package_content_file ..."
if aapt list "$apk_path" >> "$package_content_file" 2>&1
    then
	echo -e "👍  Package contents extracted.\n"
else 
	echo "‼️  ⏳ Extracting package contents failed."
	exit 1;
fi

# extract package detail badging with aapt
package_badging_file=$app_name"/badging.txt"
echo "⏳ Extracting package detail badging in $package_badging_file ..."
if aapt dump badging "$apk_path" >> "$package_badging_file" 2>&1
    then
	echo -e "👍  Package detail badging extracted.\n"
else 
	echo "‼️  ⏳ Extracting package detail badging failed."
	exit 1;
fi

# extract package detail permissions with aapt
package_permissions_file=$app_name"/permissions.txt"
echo "⏳ Extracting package detail permissions in $package_permissions_file ..."
if aapt dump permissions "$apk_path" >> "$package_permissions_file" 2>&1
    then
	echo -e "👍  Package detail permissions extracted.\n"
else 
	echo "‼️  ⏳ Extracting package permissions failed."
	exit 1;
fi

# extract package detail configurations with aapt
package_configurations_file=$app_name"/configurations.txt"
echo "⏳ Extracting package detail configurations in $package_configurations_file ..."
if aapt dump configurations "$apk_path" >> "$package_configurations_file" 2>&1
    then
	echo -e "👍  Package detail configurations extracted.\n"
else 
	echo "‼️  ⏳ Extracting package detail configurations failed."
	exit 1;
fi

# extract package detail resources with aapt
package_resources_file=$app_name"/resources.txt"
echo "⏳ Extracting package detail resources in $package_resources_file ..."
if aapt dump resources "$apk_path" >> "$package_resources_file" 2>&1
    then
	echo -e "👍  Package detail resources extracted.\n"
else 
	echo "‼️  ⏳ Extracting package detail resources failed."
	exit 1;
fi

# extract package detail xmltree with aapt
package_xmltree_file=$app_name"/xmltree.txt"
echo "⏳ Extracting package detail xmltree in $package_xmltree_file ..."
if aapt dump resources "$apk_path" >> "$package_xmltree_file" 2>&1
    then
	echo -e "👍  Package detail xmltree extracted.\n"
else 
	echo "‼️  ⏳ Extracting package xmltree failed."
	exit 1;
fi

# extract decode apk with apktool in a subshell
(
decoded_apk_file=$app_name"/decoded_apk"
cd "$app_name" || exit 1;
echo "Decoding apk in $decoded_apk_file ..."
if apktool d "$apk_name" -o decoded_apk
    then
	echo -e "👍  apk decoded.\n"
else 
	echo "‼️  Decode apk failed."
	exit 1;
fi
) 
